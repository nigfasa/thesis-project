import pandas as pd
import csv
'''
This document turn the points given by the solution of the AW algorithm and create traces.
Only useful for the QGIS reprentation. 
'''
# The algorithm 
dt=pd.read_csv("./ordering_paths/edges.txt", header=None)
dm=pd.read_csv("./ordering_paths/vertices.txt", header=None)

with open('./ordering_paths/edges.txt') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    csv1= list(csv_reader)
    with open('./ordering_paths/vertices.txt') as csv_file2:
        csv_reader2 = csv.reader(csv_file2, delimiter=',')
        csv2= list(csv_reader2)
        
        dictt = {}
        for i in csv2:
            dictt[i[0]] = [i[1], i[2]]
        print(csv1)
        for  i in csv1:
            i[1] = dictt[i[1]]
            i[2] = dictt[i[2]]

        with open('./ordering_paths/final.txt', mode='w') as csv_file3:
            csv_writer = csv.writer(csv_file3, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for i in csv1:
                csv_writer.writerow([i[0], i[1][0], i[1][1], i[2][0], i[2][1]])

        


        



