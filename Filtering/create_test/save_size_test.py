import psycopg2
import pandas as pd
import pandas.io.sql as sqlio
import numpy as np
from os import listdir, path, mkdir, getcwd
import time

#size of the test
SIZE = 150000

start = time.time()

def connect_database():
    try:
        connection = psycopg2.connect(
            user = 'nigfasa',
            password = '',
            host = "127.0.0.1",
            port = "5432",
            database = "maps")
            
        cursor = connection.cursor()
        # Print PostgreSQL Connection properties
        print ( connection.get_dsn_parameters(),"\n")

        # Print PostgreSQL version
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        print("You are connected to - ", record,"\n")   
        sql = "select * from experiment3 limit "+str(SIZE)+";"
        dat = sqlio.read_sql_query(sql, connection)
        save_trayectories(dat)


    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)

    finally:
        #closing database connection.
            if(connection):
                cursor.close()
                connection.close()
                print("PostgreSQL connection is closed")


def save_trayectories(dt):
    keep_columns = ['latitude', 'longitude', 'ts', 'time_zone', 'device_id']
    new_f = dt[keep_columns]
    path_going = getcwd()+'/create_test/test_base'+str(SIZE)+'k.csv'
    new_f.to_csv(path_going, index=None, sep=',', mode='a',index_label = True, encoding='utf-8')

connect_database()
end = time.time()
print(end - start)
