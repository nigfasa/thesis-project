import psycopg2
import copy
import pandas as pd
import sys
sys.path.append('./DouglasPeucker')
from gps_utils import rdp_mod
import pandas.io.sql as sqlio
import numpy as np
from os import listdir, path, mkdir, getcwd
import time
from scipy import stats

start = time.time()



def clean_data(dt, test=False):
    '''
    Description: Here it data is filtered and The RDP algorithm is used. 

    Input: data from the database.
    Output: Filtered data.

    Parameters: 
    - RDP parameter.
    - Error group parameter: In order to clean the data from higly erratic error this was created.

    Testing: Offers the testing option. It is advised to use it to test the parameters. 
    '''

    if test:
        # test option: is quite recommended. An example is given. 
        dt=pd.read_csv("./final_test/test_base400k.csv")


    # filter the data
    keep_columns = ['latitude', 'longitude', 'ts', 'time_zone', 'device_id']
    new_f = dt[keep_columns]


    # group them into unique trajectories
    f_grouped = dt.groupby(['device_id', 'time_zone'])
    keys = list(f_grouped.groups.keys())

    # create extra columns with the difference in distance and time between a point
    # and its sucessor.
    frames2 = []
    douglas_time = 0
    all_values=[]
    all_count=[]
    for i in keys:
        all_distances = []
        all_times = []
        all_distances.append(0)
        all_times.append(0)
        k_0 =  f_grouped.get_group(i)[['latitude', 'longitude', 'ts']]
        k_1 = list(zip(list(k_0['latitude']), list(k_0['longitude'])))
        k_t = list(k_0['ts'])
        for j,k in zip(k_1, k_1[1:]):
            distance_m = np.sqrt((j[0]-k[0])**2 + ((j[1]-k[1])**2))
            all_distances.append(distance_m)
        for j,k in zip(k_t, k_t[1:]):
            time_diff = abs(int(k)-int(j))
            all_times.append(time_diff)
        a = f_grouped.get_group(i).apply(lambda x: x)
        a.insert(2, "dif_ts", all_times, True)
        a.insert(2, "dif_dis", all_distances, True)
        
        # trajectories that are too long are splited into smaller ones:
        # In order to avoid cicles, and 
        # if a whole trajectory as too much error to erase the minimun amount of data. 
        c= copy.copy(a)
        # IMPORTANT: this value can be changed to improve results. 
        steps = 60
        divisions = [c[x:x+steps] for x in range(0, len(c), steps)]


        for a in divisions:
            # knowing that a person walks normally at 1m/s
            # if the average of a set is bigger than 3 the set has too many errors to be a good sample

            # IMPORTANT: this value can be changed to improve results. 
            max_average = 3

            ave = a['dif_dis'].mean(skipna=True)
            if ave > max_average:
                continue
            all_values.append(ave)
            coun = a['dif_dis'].count()
            if coun < 20:
                continue
            all_count.append(coun)


            # This erases points that go too fast and too slow. This 
            a = a[a['dif_ts']<1500]
            a = a[a['dif_ts']>100]
            a = a[a['dif_dis']<1.4]
            a = a[a['dif_dis']>0.1]
            k_0 = a[['longitude', 'latitude']].values
            k_t = a[['ts']].values



            # The RDP Algorithm is used here
            # IMORTANT: this value can be changed. 
            rdp_value = 1.0

            start_douglas = time.time()
            new_f = rdp_mod(k_0, k_t, rdp_value)
            end_douglas = time.time()
            douglas_difference =  end_douglas-start_douglas
            douglas_time += douglas_difference
            

            new_f = new_f.tolist()
            lon = []
            lat = []
            tps = []
            for m in new_f:
                lon.append(m[0])
                lat.append(m[1])
                tps.append(m[2])
            b = pd.DataFrame({'longitude': lon, 'latitude': lat, 'ts': tps})

            frames2.append(b)

    
    '''
    Note:
    # the folling code helps to see an stadistical analysis of the data

    print('start size:')
    print(len(dt))
    print(dt.describe())

    a_v = pd.DataFrame(all_values)
    print(a_v.describe())
    a_v = pd.DataFrame(all_count)
    print(a_v.describe())
    print(type(frames2))
    print(frames2)
    print('Douglas total time: {}'.format(str(douglas_time)))
    '''

    return(frames2)


'''
Test time:
end = time.time()
print(end - start)
'''
