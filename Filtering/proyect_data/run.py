import psycopg2
import sys
sys.path.append('./DouglasPeucker')
sys.path.append('./data_cleaning')
from data_clean2 import clean_data
from gps_utils import rdp_mod
from save_data import save_trayectories2
import pandas as pd
import pandas.io.sql as sqlio
import numpy as np
from os import listdir, path, mkdir, getcwd
import time

'''
Amount of data you need from the database. 
'''
SIZE = "5"

def connect_database():
    try:
        connection = psycopg2.connect(
            user = 'nigfasa',
            password = '',
            host = "127.0.0.1",
            port = "5432",
            database = "maps")
            
        cursor = connection.cursor()
        # Print PostgreSQL Connection properties
        print ( connection.get_dsn_parameters(),"\n")

        # Print PostgreSQL version
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        print("You are connected to - ", record,"\n")
        sql = "select * from experiment3 limit "+str(SIZE)+";"
        dat = sqlio.read_sql_query(sql, connection)
        new_data = clean_data(dat, True)
        save_trayectories2(new_data)

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)

    finally:
        #closing database connection.
            if(connection):
                cursor.close()
                connection.close()
                print("PostgreSQL connection is closed")



connect_database()
