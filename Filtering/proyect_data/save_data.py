import pandas as pd
import numpy as np
from os import listdir, path, mkdir, getcwd
import time
from scipy import stats

STEPS = str(60)


# The function saves the data into a csv with splitint trajectories into smaller onces to overcome the problem of cycles. 
# The steps variables is important, the smaller the slower the solution will be. You would always want to have really long
# perfect trajectories but is impossible. 
def save_trayectories2(list_dt):
    #start = time.time()
    key = 1
    frames = []
    for i in list_dt:
        steps = int(STEPS)
        group = i
        chunks = [group[x:x+steps] for x in range(0, len(group), steps)]
        if not(path.isdir('./proyect_data/steps_'+STEPS)):
            mkdir('proyect_data/steps_'+STEPS)
        for j in chunks:
            key_index  =  [key]*len(j)
            j.insert(0, "key_index", key_index, True)
            key+=1
            frames.append(j)
    result = pd.concat(frames)    
    path_going = getcwd()+'/proyect_data/steps_'+STEPS+'/'+str('final_result')+'.csv'
    result[['key_index','latitude', 'longitude', 'ts']].to_csv(path_going, header=None, index=None, sep=',', mode='a') 
    #print('Size of the final file: ' + str(len(result)))
    #end = time.time()
    #print("cortar en pedazos: "+ str(end-start))
