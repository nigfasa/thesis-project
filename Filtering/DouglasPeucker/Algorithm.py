
import sys
sys.path.append('./DouglasPeucker')
from gps_utils import rdp
from gps_utils import fake_print
import rdp
import gpxpy
import mplleaflet
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
plt.rcParams['axes.xmargin'] = 0.1
plt.rcParams['axes.ymargin'] = 0.1

import seaborn as sns
sns.set_style("whitegrid")
sns.set_context("talk")
fake_print()

traverse = np.array([[0., 1.], [0.3, 0.6], [2., 0.], [3.8, 0.65], [5.6, 0.9], [6.3, 0.86], [7.1, 0.61], [10., 0.95]])
fig = plt.figure()
plt.plot(traverse[:,0], traverse[:,1])
plt.plot(traverse[:,0], traverse[:,1], 'o')
fig.tight_layout()
dt=pd.read_csv("./DouglasPeucker/data_normal.csv")